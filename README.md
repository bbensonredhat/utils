# utils

Public utilities in sh, py, lua, rb, and similar interpreted langs.  Usually small or simple, but not always.  Unlikely to have config or etc files.