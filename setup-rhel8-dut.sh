#!/usr/bin/bash
#
# setup rhel 8 dut
#
# license:  GPLv2
#

### shell run options
#set -o pipefail  #return any nonzero code in pipe as retval for whole pipe
set -u  #fail+exit on any unset var
#set -e  #exit on any fail
#set -x  #debug
#IFS=$' '

## vars unexported
rethappy=0
retcode=3
scriptdir=$(dirname $0)

trap exit_cleanup EXIT  #prepare for cleanup from anywhere

# all functions called from here
main () {
    #printusage
    checkpwd
    checksudo
    $scriptdir/checkefi.sh
    retcode=$rethappy
    exit $rethappy
}

printusage () {
  echo "
   Usage:
    - no CLI input/argument is required or accepted
    - do some common stuff for a newly-installed machine
"
}

checkpwd () {
    pwd
    echo $scriptdir
}

# needs wheel-based sudoer or root
checksudo () {
    #[[ $(sudo -v) -eq 0 ]] && echo "sudo ok" || echo "no sudo"
    sudo -v && echo "sudo ok" || echo "no sudo"
}

exit_cleanup () {
    echo "exiting with $retcode"
    exit $retcode
}

### do everything
main
